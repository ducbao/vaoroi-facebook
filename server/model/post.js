'use strict';
var mongoose = require('mongoose');
var config = require('../config/environment');
var request = require('request');
var FacebookAccountModel = mongoose.model('facebook_account');

class PostModel {
  /**
   * create post
   * @param {array} groups
   * @param {string} message
   * @param {string} accessToken
   * @param {function} callback
   * @return {object}
   */
  createPost(groups, message, accessToken, callback) {
    var index = 0;
    var result = true;

    groups.forEach(function(group) {
      var postUrl = config.graphApiUrl + group.fb_id + config.feedEndPoint;;

      var param = {
        message: message,
        access_token: accessToken
      };

      request({
        uri: postUrl,
        method: 'POST',
        form: param
      }, function(error, response, body) {
        index = index + 1;
        if (error === null && typeof body === 'string') {
          var postRes = JSON.parse(body);
          console.log(postRes);

          if (index === groups.length) {
            return callback(result);
          }
        } else {
          result = false;
          console.log(error);
          if (index === groups.length) {
            return callback(result);
          }
        }
      });
    })
  }

  getListSns(callback) {
    FacebookAccountModel.find({}).exec(function (err, snsList) {
      if (err) {
        return callback([]);
      } else {
        return callback(snsList);
      }
    });
  }

  addSns(id, name, image, type, callback) {
    var snsSave = new FacebookAccountModel();

    snsSave.fb_id = id;
    snsSave.name = name;
    snsSave.image = image;
    snsSave.type = type;

    snsSave.save(function (err, saveResult) {
      if (err) {
        return callback(false);
      } else {
        return callback(true);
      }
    });
  }
}

module.exports = PostModel;
