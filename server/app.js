/**
 * Main application file
 */

'use strict';
// Set default node environment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');
var fs = require('fs');

// Setup server
var app = express();

var options = {
  key: fs.readFileSync(__dirname + '/components/certificates/server.key'),
  cert: fs.readFileSync(__dirname + '/components/certificates/server.crt')
};

var server = require('https').createServer(options, app);

var mongoose = require('mongoose');
// require('./data/FacebookUser');
require('./data/FacebookAccount.js');

mongoose.connect('mongodb://vaoroiUser:$R34dyL4unch!@188.166.191.205:26996/vaoroi');

require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function() {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// end
process.on('message', function(msg) {
  if (msg === 'shutdown') {

    console.log('Closing all connections...');

    setTimeout(function() {
      console.log('Finished closing connections');
      process.exit(0);
    }, 1500);
  }
});

// Expose app
exports = module.exports = app;
