/**
 * Express configuration
 */

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');
var session = require('express-session');
var fs = require('fs');
var pmx = require('pmx').init();

module.exports = function(app) {
  var env = app.get('env');

  app.set('views', config.root + '/server/views');
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  app.use(compression());

  app.use(bodyParser.json({
    limit: '50mb'
  }));
  app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  }));

  app.use(methodOverride());

  // keymetricにエラー情報を送る
  app.use(pmx.expressErrorHandler());
  // keymetricにHTTP latency情報を送る
  pmx.http({
    http: true,
    ignore_routes: [/socket\.io/, /notFound/]
  });

  // セッションとクッキーのセット
  app.use(cookieParser());
  app.use(session({
    secret: 'Ev9eIc4ir8uV0Nooth6Paj3varv2eV2n',
    // requestを受け付ける度にsessionを保存し直すか
    resave: true,
    saveUninitialized: true
  }));

  // 本番稼働時
  if ('production' === env) {
    app.use(favicon(path.join(config.root, 'src', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'src')));
    app.set('appPath', config.root + '/src');
    app.use(morgan('dev'));
  }

  // ステージング稼働時
  if ('stg' === env) {
    app.use(favicon(path.join(config.root, 'src', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'src')));
    app.set('appPath', config.root + '/src');
    app.use(morgan('dev'));
  }

  // 開発時
  if ('development' === env || 'test' === env) {
    app.set('tmpPath', express.static(path.join(config.root, '.tmp')));
    app.set('srcPath', express.static(path.join(config.root, 'src')));
    app.set('appPath', '.tmp');
    app.use(morgan('dev'));
    app.use(errorHandler());
  }
};