'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// developもproductionもコールされる設定[設定は最小限に]
var all = {
  env: process.env.NODE_ENV,

  //serverのルートパス
  root: path.normalize(__dirname + '/../../..'),

  //ポート
  port: process.env.PORT || 9000
};

// 環境ごとにrequireする設定ファイルの変更
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
