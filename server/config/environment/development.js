'use strict';

// 開発環境のセッティング
module.exports = {
  graphApiUrl: 'https://graph.facebook.com/v2.3/',
  feedEndPoint: '/feed'
};
