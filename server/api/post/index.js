'use strict';

var express = require('express');
var controller = require('./post.controller');

var router = express.Router();

router.post('/createPost', controller.createPost);
router.post('/addSns', controller.addSns);
router.get('/getListSns', controller.getListSns);

module.exports = router;
