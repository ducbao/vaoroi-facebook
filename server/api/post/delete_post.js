'use strict';

var moment = require('moment');
var Q = require('q');
var request = require('request');
var https = require('https');

var deferred = Q.defer();
var postIds = [];
// 投稿一覧
var postDatas = [];
var accessToken = 'CAACEdEose0cBAJVj2ygy8aPUgurKq20Xz5Ul5fQ8hibamqmXxyT9V4ZCskQFEvZBZAePKnjoZCJHwZADMx5VBLlGGsGpATc7waFZAijo7brqZCmr5Mtlcj4lHyGjumdBtJGe2qlLWWtenB0zZB3ke5s2fVjPb1Jx7QxZCjcMoqhSfhS2R0jlTm17zDBmzKtYxNsUZD';

// エンドポイント
var url = 'https://graph.facebook.com/v2.5/244735582348952/photos?type=uploaded&fields=id&limit=50&access_token=' + accessToken;

    request({
        uri: 'https://www.facebook.com/241106976238505',
        method: 'GET'
      }, function(err, body, res) {
          console.log(err);
          console.log(body);
          console.log(res);
      });
//   // 指定したアカウントの投稿一覧を取得
// getPosts(postIds, url, deferred).then(function(postDatas) {
//   console.log(postDatas);
//   // 投稿一覧がある場合、保存
//   if (postDatas !== null && postDatas.length > 0) {
//     postDatas.forEach(function(postId, index) {
//       var param = {
//         access_token: 'CAACEdEose0cBAJVj2ygy8aPUgurKq20Xz5Ul5fQ8hibamqmXxyT9V4ZCskQFEvZBZAePKnjoZCJHwZADMx5VBLlGGsGpATc7waFZAijo7brqZCmr5Mtlcj4lHyGjumdBtJGe2qlLWWtenB0zZB3ke5s2fVjPb1Jx7QxZCjcMoqhSfhS2R0jlTm17zDBmzKtYxNsUZD'
//       };

//       // 'DELETE'メソッドでPOSTリクエスト実行
//       request({
//         uri: 'https://graph.facebook.com/v2.5/' + postId,
//         method: 'DELETE',
//         form: param
//       }, function() {
//         if (index === postDatas.length - 1) {
//           console.log('finished');
//         }
//       });
//     });
//   }
// });

function getData(url, callback) {
  function tryGetData(index) {
    if (index >= 3) {
      return callback(null);
    } else {
      // getでAPIからデータを取得する
      https.get(url, function(res) {
        res.setEncoding('utf8');
        var output = '';

        // dataがある場合
        res.on('data', function(response) {
          output += response;
        });

        // 通信終了でコールバックreturn
        res.on('end', function() {
          output = JSON.parse(output);
          // データがある場合、リターン
          if ((output.meta === undefined && output.error === undefined) || (output.meta !== undefined && output.data !== undefined)) {
            return callback(output);
          } else {
            index = index + 1;
            // タイムアウトをセット
            setTimeout(function() {
              var errorInfo = output.error !== undefined ? output.error : output.meta;

              tryGetData(index);
            }, 100);
          }
        });
        // エラーの場合
      }).on('error', function() {
        index = index + 1;
        // タイムアウトをセット
        setTimeout(function() {
          tryGetData(index);
        }, 100);
      });
    }
  }

  tryGetData(0);
};

function getPosts(postIds, url, deferred) {


  // urlでpost_idリストを取得する
  getData(url, function(result) {
    if (result !== null && result.data !== undefined && result.data.length > 0) {
      // レスポンス数
      var responseCount = result.data.length;

      result.data.forEach(function(post) {
        // 戻り値にデータを追加
        postIds.push(post.id);
      });

      // レスポンス数がレスポンスリミット数より大きい場合、期間を変更して次のデータを取得する
      if (result.paging && result.paging.next) {
        getPosts(postIds, result.paging.next, deferred);
      } else {
        // レスポンス数がレスポンスリミット数より小さい場合、post_idリストをリターン
        deferred.resolve(postIds);
      }
    } else if (result !== null && result.data !== undefined && result.data.length === 0) {
      deferred.resolve(postIds);
    } else {
      deferred.resolve(postIds);
    }
  });
  return deferred.promise;
}
