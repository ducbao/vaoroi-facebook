'use strict';

var moment = require('moment');
var PostModel = require('../../model/post');

/**
 * create post
 * @param {Object} req
 * @param {Object} res
 * @return {json}
 */
exports.createPost = function createPost(req, res) {
  var post = new PostModel();
  var groups = req.body.groups;
  var message = req.body.message;
  var accessToken = req.body.accessToken;

  post.createPost(groups, message, accessToken, function(result) {
    res.json({
      result: result
    });
  });
};

exports.getListSns = function getListSns(req, res) {
  var sns = new PostModel();

  sns.getListSns(function(result) {
    res.json([{
      result: result
    }]);
  });
};

/**
 * create group
 * @param {Object} req
 * @param {Object} res
 * @return {json}
 */
exports.addSns = function addSns(req, res) {
  var post = new PostModel();
  var id = req.body.id;
  var name = req.body.name;
  var image = req.body.image;
  var type = 'group';

  post.addSns(id, name, image, type, function(result) {
    res.json({
      result: result
    });
  });
};
