'use strict';

var errors = require('./components/errors');

module.exports = function(app) {
  var env = app.get('env');

  app.use('/api/post', require('./api/post'));

  if ('development' === env || 'test' === env) {
    app.use(app.get('tmpPath'));
    app.use(app.get('srcPath'));
  }

  app.route('/:url(api|auth|components|app|vendor|assets)/*')
    .get(errors[404]);

  app.route('/*')
    .all(function(req, res) {
      res.sendFile('/main.html', {
        root: app.get('appPath')
      });
    });
};
