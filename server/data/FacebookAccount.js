var mongoose = require('mongoose');

var FacebookAccountSchema = new mongoose.Schema({
    fb_id : String,
    name : String,
    type : String,
    image : String
});

mongoose.model('facebook_account', FacebookAccountSchema);
