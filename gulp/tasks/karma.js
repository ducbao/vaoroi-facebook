'use strict';
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();

const testFiles = [
  './.tmp/lib.js',
  './.tmp/main.js',
  './.tmp/app.js',
  './libs/angular-mocks/angular-mocks.js',
  `./src/app/**/*.spec.js`
];

// Unit Test
gulp.task('karma:dev', function() {
  return gulp.src(testFiles)
    .pipe(plugins.karma({
      configFile: 'karma.conf.js',
      action: 'watch'
    }));
});