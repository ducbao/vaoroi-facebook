'use strict';
const gulp = require('gulp');
const del = require('del');

// Development
gulp.task('clean:dev', function(callback) {
  del(['./.tmp'], {
    force: true
  }, callback);
});

// Dist
gulp.task('clean:dist', function(callback) {
  del(['dist'], callback);
});