'use strict';
const gulp = require('gulp');
const templateCache = require('gulp-angular-templatecache');

gulp.task('template', () => {
  gulp.src('.tmp/app/**/*.html')
    .pipe(templateCache({
      module: 'vaoroiApp'
    }))
    .pipe(gulp.dest('.tmp'));
});
