'use strict';
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const jade = require('jade');

// jade
gulp.task('jade', function() {
  gulp.src('src/**/*.jade')
    .pipe(plugins.cached('jade'))
    .pipe(plugins.plumber())
    .pipe(plugins.jade({
      jade: jade,
      pretty: true
    }))
    .pipe(gulp.dest('.tmp'));
});