'use strict';
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const browserSync = require('browser-sync');
const reload = browserSync.reload;

// Development
gulp.task('scss:dev', () => {
  return gulp.src('src/scss/*.scss')
    // .pipe(plugins.cached('scss'))
    .pipe(plugins.plumber())
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
      includePaths: ['libs/mdi/scss/']
    }))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({
      stream: true
    }));
});