'use strict';
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();

// Development
gulp.task('script:dev', function() {
  return gulp.src(['src/app/**/*.js', '!src/app/main.js', '!src/app/start.js', '!src/app/**/*.spec.js'])
    // .pipe(plugins.cached('script'))
    .pipe(plugins.plumber())
    .pipe(plugins.sourcemaps.init())
    // ファイルを結合
    .pipe(plugins.concat('app.js'))
    // ES6 to ES5
    .pipe(plugins.babel())
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp'));
});

gulp.task('script:main', function() {
  return gulp.src(['src/app/main.js'])
    // .pipe(plugins.cached('script'))
    .pipe(plugins.plumber())
    .pipe(plugins.sourcemaps.init())
    // ES6 to ES5
    .pipe(plugins.babel())
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp'));
});

gulp.task('script:start', function() {
  return gulp.src(['src/app/start.js'])
    // .pipe(plugins.cached('script'))
    .pipe(plugins.plumber())
    .pipe(plugins.sourcemaps.init())
    // ES6 to ES5
    .pipe(plugins.babel())
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp'));
});