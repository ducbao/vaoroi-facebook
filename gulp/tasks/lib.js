'use strict';
const gulp = require('gulp');
const source = require('vinyl-source-stream');
const browserify = require('browserify');

// 外部ライブラリを結合
gulp.task('libs', function() {
  browserify({
      entries: ['./src/lib.js']
    })
    .bundle()
    .pipe(source('lib.js'))
    .pipe(gulp.dest('./.tmp'));
});