'use strict';
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();

// Development
gulp.task('copy:dev', function() {
  return gulp.src(['./src/fonts/**/*', './src/images/**/*'], {
      base: './src'
    })
    .pipe(plugins.plumber())
    .pipe(gulp.dest('.tmp'));
});