'use strict';

// browserifyでrequireするライブラリをここに定義
global.$ = global.jQuery = require('jquery');
global.moment = require('moment');
global._ = require('lodash');
require('angular');
require('angular-ui-router');
require('angular-ui-select');
require('angular-animate');
require('angular-aria');
require('angular-material');
require('restangular');
require('angular-messages');
require('angular-cookies');
require('angular-easyfb');
