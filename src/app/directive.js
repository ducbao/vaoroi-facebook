'use strict';

angular.module('vaoroiApp')

.directive('mainNav', () => {
  return {
    link: ($scope) => {

      $scope.openMenu = function($mdOpenMenu, ev) {
        $mdOpenMenu(ev);
      };
    }
  };
});
