'use strict';

angular.module('vaoroiApp', [
  'ui.router',
  'ngMaterial',
  'restangular',
  'ngCookies',
  'ngMessages',
  'ezfb'
])

.run(function($state, $rootScope) {
  $rootScope.$state = $state;
})

.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/post');
})

.config(function($locationProvider, $httpProvider) {
  $locationProvider.html5Mode(true);

  if (!$httpProvider.defaults.headers.get) {
    $httpProvider.defaults.headers.get = {};
  }

  $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
});
