'use strict';
angular.module('vaoroiApp')

.directive('snsModal', function () {
  return {
    restrict: 'A',
    controller:($rootScope, $scope, $q, $timeout, PostService, $mdDialog, ezfb, DialogService) => {
      // get list page, group
      PostService.getSns().then(function(sns) {
        $scope.searchSns = sns;
      });

      $scope.createFlg = false;

      $scope.changeCreateButton = function(createFlg) {
        $scope.createFlg = !createFlg;
      }

      $scope.searchArrayData = function(array, searchObj) {

        var arrayLength = array.length;

        for (var i = 0; i < arrayLength; i = i + 1) {
          if (array[i] === searchObj) {
            return i;
          }
        }

        return -1;
      };

      // sns search
      $scope.pushEnterEvent = function(searchString, event) {
        // enter event
        if (event.which === 13) {
          $scope.keyword = searchString;

          $scope.searchDatas = [];

          if (searchString && searchString.length !== 0) {
            //  access token
            var accessToken = '555271474643971|b8b6ca17083d48beeedeff81e02d5d50';

            // URL search
            ezfb.api('/search?q="' + searchString + '"&type=group&fields=icon,name,id&limit=20&access_token=' + $rootScope.accessToken, function(result) {
              if (result['data']) {
                var searchData = [];

                // 検索結果分データを繰り返す
                var resultLength = result['data'].length;
                for (var i = 0; i < resultLength; i++) {

                  // 検索ページが追加されていない場合
                  if ($scope.searchArrayData($scope.searchSns, result['data'][i]['id']) < 0) {

                    result['data'][i]['image'] = result['data'][i]['icon'];
                    searchData.push(result['data'][i]);
                  }
                }

                if (searchData.length > 10) {
                  searchData.length = 10;
                }
                $scope.searchDatas = searchData;

                if ($scope.keyword === '') {
                  $scope.searchDatas = [];
                }
              }
            });

          } else {
            $scope.searchDatas = [];
          }
        }
      };

      $scope.addAccount = function(searchData) {
        var addFlg = true;

        var setSnsLength = $scope.searchSns.length;

        for (var i = 0; i < setSnsLength; i = i + 1) {

          if ($scope.searchSns[i]['fb_id'] === searchData['id']) {
            addFlg = false;
            break;
          }
        }

        if (addFlg) {
          var addAccountData = {};
          addAccountData.id = searchData['id'];
          addAccountData.name = searchData['name'];
          addAccountData.image = searchData['image'];
          addAccountData.addFlg = 1;

          PostService.addSns(addAccountData).then(function(result) {
            if (result.result === true) {
              DialogService.showToast('success', 'success-toast');
              console.log(result);
              $scope.searchSns.push(addAccountData);
            } else {
              DialogService.showToast('error', 'error-toast');
            }
          });
        } else {
          DialogService.showToast('Group này đã được tạo', 'error-toast');
        }
      };

      // close modal
      $scope.close = function() {
        $mdDialog.hide();
      };
    }
  }
});
