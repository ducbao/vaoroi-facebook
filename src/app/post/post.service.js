'use strict';

/**
 * PostService
 * @param  {Function} Restangular Restangular
 * @param  {Function} $q Promise
 * @return {Object}
 */
function PostService($rootScope, Restangular, $q) {
  function getSns() {
    var deferred = $q.defer();

    Restangular.all('post/getListSns').getList().then(function(sns) {
      deferred.resolve(sns[0].result);
    });

    return deferred.promise;
  }

  /**
   * createPost
   * @param {object} startEndDate
   * @return {Object}
   */
  function createPost(snsLists, message) {
    var deferred = $q.defer();

    var postInfo = {
      groups: snsLists,
      message: message,
      accessToken: $rootScope.accessToken
    };

    Restangular.all('post/createPost').post(postInfo).then(function(result) {
      deferred.resolve(result);
    });

    return deferred.promise;
  }

   // add group
  function addSns(snsInfo) {
    var deferred = $q.defer();

    Restangular.all('post/addSns').post(snsInfo).then(function(result) {
      deferred.resolve(result);
    });

    return deferred.promise;
  }

  return {
    getSns: getSns,
    createPost: createPost,
    addSns: addSns
  };
}

// 投稿表示機能用のサービスを定義
angular
  .module('vaoroiApp')
  .factory('PostService', PostService)
