'use strict';
angular.module('vaoroiApp')

/**
 * @ngdoc PostCtrl
 * @name PostCtrl
 * @requires PostService
 * @description
 **/
.controller('PostCtrl', function($rootScope, $scope, $q, $timeout, $mdDialog, PostService, DialogService, ezfb) {
  $scope.postAccount = {
    selected : []
  }

  // get list page, group
  PostService.getSns().then(function(sns) {
    $scope.selectAccountList = sns;
  });


  // add page, group
  $scope.addSns = function() {
    var modalScope = $rootScope.$new();

    // show modal
    $mdDialog.show({
      templateUrl: 'app/components/sns-modal/sns-modal.html',
      scope: modalScope,
      clickOutsideToClose: true
    });
  }

  // create post
  $scope.createPost = function () {
    PostService.createPost($scope.postAccount.selected, $scope.message).then(function(result) {
      if (result.result) {
        DialogService.showToast('Đăng bài thành công', 'success-toast');
      } else {
        DialogService.showToast('Có bài đăng bị lỗi', 'error-toast');
      }
    });
  };

  // get accessToken of user
  $scope.fbConnectLogin = function() {
    ezfb.getLoginStatus(function(res) {
      if (res.authResponse === undefined || res.status === 'not_authorized' || res.status === 'unknown') {
        // login
        ezfb.login(function(res) {
          if (res.authResponse !== undefined) {
            // get access token
            $rootScope.accessToken = res.authResponse.accessToken;
            DialogService.showToast('Tạo access token thành công', 'success-toast');
          }
        }, {
          scope: 'publish_actions'
        });
      } else if (res.authResponse !== undefined && res.status === 'connected') {
        // get access token
        DialogService.showToast('Tạo access token thành công', 'success-toast');
        $rootScope.accessToken = res.authResponse.accessToken;
      }
    }, {
      scope: 'publish_actions'
    });
  };
});
