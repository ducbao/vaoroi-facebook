'use strict';

angular.module('vaoroiApp', [
  'ui.router',
  'ngMaterial',
  'restangular',
  'ngCookies',
  'ui.select',
  'ezfb'
])

.run(function($state, $rootScope) {
  $rootScope.$state = $state;
})

// setting state
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/post');

  // define state of page
  $stateProvider
    .state('post', {
      url: '/post',
      templateUrl: 'post/post.html',
      controller: 'PostCtrl',
      title: 'Post'
    })
    .state('sns', {
      url: '/sns',
      templateUrl: 'sns/sns.html',
      controller: 'SnsCtrl',
      title: 'Sns'
    })
    .state('sns-modal', {
      url: '/sns-modal',
      templateUrl: 'sns-modal/sns-modal.html',
      controller: 'snsModalCtrl',
      title: 'snsModal'
    });
})

// other
.config(function($locationProvider, $httpProvider) {
  $locationProvider.html5Mode(true);

  if (!$httpProvider.defaults.headers.get) {
    $httpProvider.defaults.headers.get = {};
  }

  $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
})

/**
 * Setting RestAngular
 * @param {*} RestangularProvider
 * @return {void}
 **/
.config(function(RestangularProvider) {
  RestangularProvider.setBaseUrl('/api/');
})

// Setting Angular Material
.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo', {
      'default': '500'
    })
    .accentPalette('indigo', {
      'default': '500'
    });
})

// setting facebook
.config(function(ezfbProvider) {
  // Facebook APPID
  var appId = '555271474643971';

  ezfbProvider.setInitParams({
    appId: appId
  });
});
