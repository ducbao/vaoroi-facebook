'use strict';

/**
 * DialogService ダイアログ表示用のサービス
 * @param  {Function} $mdDialog ダイアログ表示用
 * @return {Object} オブジェクト
 */
function DialogService($mdDialog, $mdToast) {
  /**
   * showError エラーを表示するダイアログ
   * @param {String} errTitle エラーのタイトル
   * @param {String} errMessage エラーの内容
   * @return {void}
   */
  function showError(errTitle, errMessage) {
    $mdDialog.show(
      $mdDialog.alert()
      .parent(angular.element(document.querySelector('#popupContainer')))
      .clickOutsideToClose(true)
      .title(errTitle)
      .textContent(errMessage)
      .ok('閉じる')
    );
  }
  /**
   * showModal モーダルを表示するダイアログ
   * @param {String} templateUrl モーダルのテンプレートURL
   * @param {Object} parentScope モーダルに渡すスコープ
   * @return {void}
   */
  function showModal(templateUrl, parentScope) {
    $mdDialog.show({
      templateUrl: templateUrl,
      scope: parentScope,
      preserveScope: true,
      clickOutsideToClose: true
    });
  }

  function showToast(message, toastType) {
    $mdToast.show(
      $mdToast.simple()
      .textContent(message)
      .theme(toastType)
      .position('top right')
      .hideDelay(3000)
    );
  }

  return {
    // エラーを表示するダイアログ
    showError: showError,
    showModal: showModal,
    showToast: showToast
  };
}

// ダイアログ表示用のサービスを定義
angular
  .module('vaoroiApp')
  .factory('DialogService', DialogService);
