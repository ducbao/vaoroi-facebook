'use strict';
const gulp = require('gulp');
const requireDir = require('require-dir');
const plugins = require('gulp-load-plugins')();
const runSequence = require('run-sequence');
const browserSync = require('browser-sync');
const pm2 = require('pm2');

// 外部ファイル化したタスクの読み込み
requireDir('./gulp/tasks', {
  recurse: true
});

// PM2サーバー
gulp.task('pm2', function() {
  pm2.connect(true, function() {
    pm2.start({
      // サーバースクリプト
      script: './server/app.js',
      // 変更を検知してリロード
      watch: ['./server'],
      // 環境変数
      env: {
        NODE_ENV: 'development'
      }
    }, function() {
      // コンソールにログ出力
      pm2.streamLogs('all', 0);
    });
  });
});

// 開発用サーバーを起動
gulp.task('serve:dev', () => {
  // タスクを順番に実行
  runSequence(
    'clean:dev', ['build:dev', 'pm2'], ['template', 'browsersync', 'watch']
  );
});

// ブラウザ自動リロード
gulp.task('browsersync', () => {
  browserSync.init(null, {
    proxy: 'https://localhost:9000',
    https: true,
    port: 37000,
    startPath: './',
    watchTask: true,
    ghostMode: false
  });
});

// browsersyncのリロードタスク
gulp.task('reload', function() {
  plugins.wait(100);
  browserSync.reload();
});

// ファイルの変更を監視
gulp.task('watch', () => {
  // ビルド
  gulp.watch('src/app/main.js', ['script:main']);
  gulp.watch('src/app/start.js', ['script:start']);
  gulp.watch('src/app/**/*.js', ['script:dev']);
  gulp.watch('src/**/*.scss', ['scss:dev']);
  gulp.watch('src/**/*.jade', ['jade']);

  // リロード
  gulp.watch('.tmp/**/*.js', ['reload']);
  gulp.watch('.tmp/**/*.html', ['reload', 'template']);
});

// ビルドタスク
gulp.task('build:dev', ['jade', 'scss:dev', 'script:main', 'script:start', 'script:dev', 'libs', 'copy:dev']);

// デフォルトは開発サーバーを起動
gulp.task('default', ['serve:dev']);
